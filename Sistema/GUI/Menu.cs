﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.GUI
{
    public partial class Menu : Form
    {

        //Muestra sólo los labels de Descripcion y Menu para el evento mouseEnter
        //También pone el focus en el botón que activó el evento
        void soloLabels(Object send, String lblMen, String lblDesc)
        {
            Button btn = (Button)send;
            btn.Focus();
            lblDescripcion.Text = lblDesc;
            lblMenu.Text = lblMen;
            flpGestiones.Visible = false;
            flpReportes.Visible = false;
            lblMenu.Visible = true;
            lblDescripcion.Visible = true;
        }

        //Muestra sólo los labels de Descripcion y Menu para el evento Enter
        //que se activa con el tab
        void soloLabels(String lblMen, String lblDesc)
        {
            lblDescripcion.Text = lblDesc;
            lblMenu.Text = lblMen;
            flpGestiones.Visible = false;
            flpReportes.Visible = false;
            lblMenu.Visible = true;
            lblDescripcion.Visible = true;
        }

        //Muestra sólo el FlowLayout con las gestiones
        void soloGestiones()
        {
            flpGestiones.Visible = true;
            flpReportes.Visible = false;
            lblMenu.Visible = false;
            lblDescripcion.Visible = false;
        }

        //Muestra sólo el FlowLayout con los reportes
        void soloReportes()
        {
            flpReportes.Visible = true;
            flpGestiones.Visible = false;
            lblMenu.Visible = false;
            lblDescripcion.Visible = false;
        }

        public Menu()
        {
            InitializeComponent();
        }

        //-------MOUSE ENTER

        private void btnConsultas_MouseEnter(object sender, EventArgs e)
        {
            soloLabels(
                sender, 
                "Consultas", 
                "Gestión de las consultas y procedimientos. Muestra cada una de las " +
                "consultas registradas, se pueden filtrar dependiendo del campo necesario." +
                " También se puede agregar más consultas o procedimientos a realizar. " +
                "\n\nClic para abrir.");
        }

        private void btnVentas_MouseEnter(object sender, EventArgs e)
        {
            soloLabels(
                sender, 
                "Ventas", 
                "Gestión de las ventas. Muestra cada una de las ventas registradas, " +
                "se pueden filtrar dependiendo del campo necesario. También se puede " +
                "realizar más ventas. \n\nClic para abrir.");
        }

        private void btnGestiones_MouseEnter(object sender, EventArgs e)
        {
            soloLabels(
                sender, 
                "Gestiones",
                "Gestión de las entidades del sistema. Muestra cada grupo " +
                "de registros que pueden ser modificados de alguna manera, " +
                "ya sea con inserciones, con modificaciones o con eliminaciones. " +
                "\n\nClic para mostrar cada gestión.");
        }

        private void btnCompras_MouseEnter(object sender, EventArgs e)
        {
            soloLabels(
                sender,
                "Compras",
                "Gestión de las entidades del sistema. Muestra cada grupo de registros " +
                "que pueden ser modificados de alguna manera, ya sea con inserciones, " +
                "con modificaciones o con eliminaciones. \n\nClic para abrir.");
         }

        private void btnReportes_MouseEnter(object sender, EventArgs e)
        {
            soloLabels(
                sender, 
                "Reportes", 
                "Gestión de las entidades del sistema. Muestra cada grupo de " +
                "registros que pueden ser modificados de alguna manera, ya sea " +
                "con inserciones, con modificaciones o con eliminaciones. " +
                "\n\nClic mostrar cada tipo de registro.");
        }



        //-------ENTER

        private void btnConsultas_Enter(object sender, EventArgs e)
        {
            soloLabels(
                "Consultas", 
                "Gestión de las consultas y procedimientos. Muestra cada una de las consultas registradas, " +
                "se pueden filtrar dependiendo del campo necesario. También se puede agregar más consultas o " +
                "procedimientos a realizar. Clic para abrir.");
        }

        private void btnVentas_Enter(object sender, EventArgs e)
        {
            soloLabels(
                "Ventas", 
                "Gestión de las ventas. Muestra cada una de las ventas registradas, se pueden filtrar " +
                "dependiendo del campo necesario. También se puede realizar más ventas. \n\nClic para abrir.");
        }

        private void btnGestiones_Enter(object sender, EventArgs e)
        {
            soloLabels(
                "Gestiones", 
                "Gestión de las entidades del sistema. Muestra cada grupo de registros " +
                "que pueden ser modificados de alguna manera, ya sea con inserciones, con " +
                "modificaciones o con eliminaciones. \n\nClic para mostrar cada gestión.");
        }

        private void btnCompras_Enter(object sender, EventArgs e)
        {
            soloLabels(
                "Compras", 
                "Gestión de las entidades del sistema. Muestra cada grupo de " +
                "registros que pueden ser modificados de alguna manera, ya sea con " +
                "inserciones, con modificaciones o con eliminaciones. \n\nClic para abrir.");
        }

        private void btnReportes_Enter(object sender, EventArgs e)
        {
            soloLabels(
                "Reportes", 
                "Gestión de las entidades del sistema. Muestra cada grupo de " +
                "registros que pueden ser modificados de alguna manera, ya sea " +
                "con inserciones, con modificaciones o con eliminaciones. " +
                "\n\nClic mostrar cada tipo de registro.");
        }


        //-------CLICK

        

        private void btnReportes_Click(object sender, EventArgs e){ soloReportes(); }

        private void btnGestiones_Click(object sender, EventArgs e) { soloGestiones(); }

        private void btnGestionMedicos_Click(object sender, EventArgs e)
        {
            General.GUI.Medicos.GestionMedicos f = new General.GUI.Medicos.GestionMedicos();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void btnGestionEspecialidades_Click(object sender, EventArgs e)
        {
            General.GUI.Especialidades.GestionEspecialidades f = new General.GUI.Especialidades.GestionEspecialidades();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void btnGestionOpciones_Click(object sender, EventArgs e)
        {
            General.GUI.Opciones.GestionOpciones f = new General.GUI.Opciones.GestionOpciones();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void btnGestionPermisos_Click(object sender, EventArgs e)
        {
            General.GUI.Permisos.GestionPermisos f = new General.GUI.Permisos.GestionPermisos();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void btnGestionCargos_Click(object sender, EventArgs e)
        {
            General.GUI.Cargos.GestionCargos f = new General.GUI.Cargos.GestionCargos();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void btnGestionPacientes_Click(object sender, EventArgs e)
        {
            General.GUI.Pacientes.GestionPacientes f = new General.GUI.Pacientes.GestionPacientes();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void btnGestionRoles_Click(object sender, EventArgs e)
        {
            General.GUI.Roles.GestionRoles f = new General.GUI.Roles.GestionRoles();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void btnGestionOperaciones_Click(object sender, EventArgs e)
        {

        }

        private void btnGestionUsuarios_Click(object sender, EventArgs e)
        {
            General.GUI.Usuarios.GestionUsuarios f = new General.GUI.Usuarios.GestionUsuarios();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void btnGestionEmpleados_Click(object sender, EventArgs e)
        {
            General.GUI.Empleados.GestionEmpleados f = new General.GUI.Empleados.GestionEmpleados();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            General.GUI.Constancias.CapDatIncapacidad f = new General.GUI.Constancias.CapDatIncapacidad();
            //f.ShowDialog();
            //this.Close();
            //f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Informes.GUI.crVentas f = new Informes.GUI.crVentas();
            f.MdiParent = this.MdiParent;
            f.Show();
            this.Close();
        }
    }
}
