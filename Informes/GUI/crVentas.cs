﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Informes.GUI
{
    public partial class crVentas : Form
    {
        private void CargarReporte()
        {
            REP.crVentas oReporte = new REP.crVentas();
            oReporte.SetDataSource(CacheManager.SystemCache.ReporteVentas(Convert.ToDateTime(dtpFechaInicio.Text), Convert.ToDateTime(dtpFechaFin.Text)));
            crvVentas.ReportSource = oReporte;
        }

        public crVentas()
        {
            InitializeComponent();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            CargarReporte();
        }
    }
}
