﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheManager
{
    public static class SystemCache
    {
        public static DataTable ValidarUsuario(String pUsuario, String pClave)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT
	                a.idusuario,
                    a.usuario,
                    a.idrol, b.rol,
                    c.idempleado,
                    concat(c.nombres, ' ', c.apellidos) as empleado
                FROM usuarios a, roles b, empleados c, usuarios_empleados d
                WHERE 
	                a.idrol = b.idrol 
                    AND a.idusuario = d.idusuario 
                    AND c.idempleado = d.idempleado
                    AND usuario = '" + pUsuario + "' AND credencial = sha2('" + pClave + "', 256);");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable ValidarMedico(String pUsuario, String pClave)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT
	                a.idusuario,
                    a.usuario,
                    a.idrol, b.rol,
                    c.jvpm,
                    concat(c.nombres, ' ', c.apellidos) as medico
                FROM usuarios a, roles b, medicos c, usuarios_medicos d
                WHERE 
	                a.idrol = b.idrol 
                    AND a.idusuario = d.idusuario 
                    AND c.idmedico = d.idmedico
                    AND usuario = '" + pUsuario + "' AND credencial = sha2('" + pClave + "', 256);");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable getUsuario(String pUsuario, String pCredencial, String pIdRol, String pEstado)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append("SELECT idusuario FROM usuarios WHERE");
            Sentencia.Append(" usuario = '" + pUsuario + "'");
            Sentencia.Append(" AND credencial = sha1('" + pCredencial + "')");
            Sentencia.Append(" AND idrol = '" + pIdRol + "'");
            Sentencia.Append(" AND estado = '" + pEstado + "';");

            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }       

        public static DataTable TodosMedicos()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT
                    a.idmedico,
	                a.jvpm,
                    a.nombres,
                    a.apellidos,
                    a.genero,
                    a.fecha_nacimiento,
                    a.municipio,
                    a.direccion,
                    a.dui,
                    a.nit,
                    a.fechacontratacion,
                    a.fechasalida,
                    a.estado,
                    a.iddepartamento,
                    concat(a.direccion,', ',a.municipio,', ', (SELECT departamento FROM departamentos WHERE iddepartamento = a.iddepartamento)) as dire,
                    (SELECT (`AUTO_INCREMENT`) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'medicenter' AND TABLE_NAME   = 'medicos') as 'idpropietario',
                    (SELECT (`AUTO_INCREMENT`) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'medicenter' AND TABLE_NAME   = 'contactos') as 'idcontacto'
                FROM medicos a;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TodasEspecialidades()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select idespecialidad, especialidad from especialidades;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TodosDepartamentos()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT iddepartamento, departamento FROM departamentos;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TodosRoles()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT idrol, rol FROM roles;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TodosContactos()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT idcontacto, tipo, contacto FROM contactos;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }
    
        public static DataTable IDContactosABorrarOModificar(int idMedico)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select a.idcontacto, b.tipo, b.contacto  from contactos_medico a, contactos b  where 
                a.idcontacto = b.idcontacto and 
                idmedico = " + idMedico + ";");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable IDContactosABorrarOModificarEmpleados(int idEmpleado)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select a.idcontacto, b.tipo, b.contacto  from contactos_empleado a, contactos b  where 
                a.idcontacto = b.idcontacto and 
                idempleado = " + idEmpleado + ";");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable EspecialidadesAModificar(int idMedico)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select a.idespecialidad, b.especialidad  from especialidades_medico a, especialidades b  
                where a.idespecialidad = b.idespecialidad and idmedico = " + idMedico + ";");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TodasOpciones()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT idopcion, opcion, clasificacion FROM opciones;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable PermisosUsuario(String pUsuario)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"select d.idopcion, d.opcion from usuarios a, permisos c, 
            opciones d where a.usuario='" + pUsuario + @"' and
            a.idrol = c.idrol and c.idopcion = d.idopcion;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TodosRolesPermisos()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"select idrol, rol from roles order by rol asc;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable ValidacionPermisos(String pIdRol)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"
                select if ((select idpermiso from permisos x where x.idopcion = a.idopcion and x.idrol = " + pIdRol + @") 
                is null, 0, 1) as Asignado, 
                ifnull((select idpermiso from permisos x where x.idopcion = a.idopcion and x.idrol = " + pIdRol + @"), 0) 
                as IDAsignacion, 
                a.idopcion, a.opcion, a.clasificacion from opciones a;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static Boolean EliminarPermiso(int idPermiso)
        {
            Boolean p = true; 
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"delete from permisos where 
                idpermiso = " + idPermiso + ";");
            try
            {
                oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                p = false;
            }
            return p;
        }

        public static Boolean InsertarPermiso(int idRol, int idOpcion)
        {
            Boolean p = true;
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"insert into permisos(idrol, idopcion) values(" + idRol + ", " + idOpcion + ");");
            try
            {
                oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                p = false;
            }
            return p;
        }

        public static DataTable TodosCargos()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select idcargo, cargo from cargos;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TodosPacientes()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select idpaciente, 
		        expediente, 
		        nombres, 
		        apellidos, 
		        fecha_nacimiento,  
                genero,
		        telefono, 
		        email, 
		        municipio, 
		        iddepartamento,
                concat(a.municipio,', ', (SELECT departamento FROM departamentos WHERE iddepartamento = a.iddepartamento)) as direccion
                from pacientes a;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TodosUsuarios()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select a.idusuario, 
		            a.usuario,
                    a.idrol,
                    a.estado,
                    b.rol,
                    (SELECT (`AUTO_INCREMENT`) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'medicenter' AND TABLE_NAME   = 'usuarios') as 'idusuarioNuevo',
                    if ((select x.idusuario from usuarios_empleados x where x.idusuario = a.idusuario) 
					is null, 0, 1) as 'Empleado',
                    if ((select x.idusuario from usuarios_medicos x where x.idusuario = a.idusuario) 
					is null, 0, 1) as 'Medico',
                    (select g.idmedico from usuarios_medicos g where g.idusuario = a.idusuario)
                    as 'idmedico',                    
                    (select concat(j.nombres, ' ', j.apellidos) from usuarios_medicos g, medicos j 
                    where g.idusuario = a.idusuario and g.idmedico = j.idmedico) as 'propietarioMedico',
                    (select g.idempleado from usuarios_empleados g where g.idusuario = a.idusuario)
                    as 'idempleado',
                    (select concat(j.nombres, ' ', j.apellidos) from usuarios_empleados g, empleados j 
                    where g.idusuario = a.idusuario and g.idempleado = j.idempleado) as 'propietarioEmpleado'
                    from usuarios a, 
                    roles b where a.idrol = b.idrol;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TodosEmpleados()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT
                    a.idempleado,
                    a.nombres,
                    a.apellidos,
                    a.genero,
                    a.fecha_nacimiento,
                    a.dui,
                    a.nit,
                    a.municipio,
                    a.direccion,
                    a.idcargo,
                    b.cargo,
                    a.fechacontratacion,
                    a.fechasalida,
                    a.estado,
                    a.iddepartamento,
                    concat(a.direccion,', ',a.municipio,', ', (SELECT departamento FROM departamentos WHERE iddepartamento = a.iddepartamento)) as dire,
                    (SELECT (`AUTO_INCREMENT`) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'medicenter' AND TABLE_NAME   = 'empleados') as 'idpropietario',
                    (SELECT (`AUTO_INCREMENT`) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'medicenter' AND TABLE_NAME   = 'contactos') as 'idcontacto'
                FROM empleados a, cargos b where a.idcargo = b.idcargo;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static Boolean ValidarTxbUsuario(String user)
        {
            Boolean p = false;
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select idusuario, usuario from usuarios where usuario = '" + user + "';");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch{}

            if (Convert.ToInt32(Resultado.Rows.Count.ToString()) > 0)
            {
                p = true;
            }
            return p;
        }

        public static DataTable IncapacidadMedica(String pPaciente, int pidTitular, int pIncapacidad)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select '" + pPaciente + @"' as 'nombrePaciente', b.diagnostico, c.jvpm, 					
					if((select genero from medicos where idmedico = b.idmedico) = 'FEMENINO', 'Dra.', 'Dr.') as 'abreviacion', 
					concat(c.nombres, ' ', c.apellidos) as 'nombreMedico', 
                    '" + pIncapacidad + @"' as 'incapacidad'
                    from detalle_consulta b,
                    medicos c, operaciones d where d.idtitular = " + pidTitular + @" and
					b.idconsulta = (select idoperacion from operaciones where idtitular = "+ pidTitular + @" order by idoperacion desc limit 1) and
                    d.idoperacion = b.idconsulta and c.idmedico = b.idmedico;");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable EdadPaciente(String pPaciente, int pidMedico, int pidPaciente)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"select '" + pPaciente + @"' as 'nombrePaciente', b.jvpm, 					
		            if((select genero from medicos where idmedico = " + pidMedico + @") = 'FEMENINO', 'Dra.', 'Dr.') as 'abreviacion',
                    concat(b.nombres, ' ', b.apellidos) as 'nombreMedico',
		            truncate((year(now()) - year(a.fecha_nacimiento)), 0) as 'Edad' from pacientes a, medicos b where 
                    a.idpaciente = " + pidPaciente + @" and
                    b.idmedico = " + pidMedico + ";");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable ReporteVentas(DateTime pFechaInicio, DateTime pFechaFin)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(
                @"SELECT 
                    a.idoperacion,
                    c.fecha,
                    b.nombre,
                    b.alias,
                    a.cantidad,
                    a.precioventa,
                    a.gravado
                FROM detalles_operacion a,
	                consumibles b,
                    operaciones c
                WHERE
	                a.idoperacion IN (SELECT x.idoperacion FROM operaciones x WHERE x.categoria = 'VENTA')
                    and a.idoperacion = c.idoperacion
                    and a.idconsumible = b.idconsumible
                    and date(c.fecha) between '" + pFechaInicio.ToString("yyyy/MM/dd") + "' and '" + pFechaFin.ToString("yyyy/MM/dd") + "';");
            try
            {
                Resultado = oConsulta.EjecutarConsulta(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }
    }
}
