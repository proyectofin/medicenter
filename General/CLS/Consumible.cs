﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Consumible
    {
        int _IDConsumible;
        String _Categoria;
        String _Nombre;
        String _Alias;
        int _IDMarca;
        Double _PrecioUnitario;

        public int IDConsumible { get => _IDConsumible; set => _IDConsumible = value; }
        public string Categoria { get => _Categoria; set => _Categoria = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Alias { get => _Alias; set => _Alias = value; }
        public int IDMarca { get => _IDMarca; set => _IDMarca = value; }
        public double PrecioUnitario { get => _PrecioUnitario; set => _PrecioUnitario = value; }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("INSERT INTO consumibles (categoria, nombre, alias, idmarca, preciounitario) VALUES (");
            Sentencia.Append("'" + _Categoria + "',");
            Sentencia.Append("'" + _Nombre + "',");
            Sentencia.Append("'" + _Alias + "',");
            Sentencia.Append("'" + _IDMarca + "',");
            Sentencia.Append("'" + _PrecioUnitario + "');");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE consumibles SET ");
            Sentencia.Append("categoria='" + _Categoria + "',");
            Sentencia.Append("nombre='" + _Nombre + "',");
            Sentencia.Append("alias='" + _Alias + "',");
            Sentencia.Append("idmarca='" + _IDMarca + "',");
            Sentencia.Append("preciounitario='" + _PrecioUnitario + "' WHERE idconsumible='" + _IDConsumible + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM consumibles ");
            Sentencia.Append("WHERE idconsumible='" + _IDConsumible + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
