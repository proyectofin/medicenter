﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.Proveedores
{
    public partial class GestionProveedores : Form
    {
        BindingSource _Proveedores = new BindingSource();

        private void CargarProveedores()
        {
            try
            {
                _Proveedores.DataSource = CacheManager.SystemCache.TodosProveedores();
                FiltrarLocalmente();
            }
            catch { }
        }
        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Proveedores.Filter = "proveedor LIKE '%" + txbFiltro.Text + "%'";
                    lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros encontrados";
                }
                else
                {
                    _Proveedores.RemoveFilter();
                    dtgvDatos.AutoGenerateColumns = false;
                    dtgvDatos.DataSource = _Proveedores;
                    lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros encontrados";
                }
            }
            catch { }
        }

        public GestionProveedores()
        {
            InitializeComponent();
        }

        private void GestionProveedores_Load(object sender, EventArgs e)
        {
            CargarProveedores();
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
