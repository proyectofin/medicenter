﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionExamen : Form
    {
        bool Agregar = false;

        public bool Agregar1 { get => Agregar; set => Agregar = value; }

        public EdicionExamen()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void EdicionExamen_Load(object sender, EventArgs e)
        {
            cmbEstado.DataSource = General.CLS.cmbEstados.EstadoExamen();
            cmbEstado.DisplayMember = "Dmember";
            cmbEstado.ValueMember = "Vmember";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Close();
            Agregar1 = true;
        }
    }
}
