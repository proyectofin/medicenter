﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.Marcas
{
    public partial class GestionMarcas : Form
    {
        BindingSource _Marcas = new BindingSource();
        private void CargarDatos()
        {
            try
            {
                _Marcas.DataSource = CacheManager.SystemCache.TodasMarcas();
                FiltrarLocalmente();
            }
            catch { }
        }
        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Marcas.Filter = "marca LIKE '%" + txbFiltro.Text + "%'";
                    lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros encontrados";
                }
                else
                {
                    _Marcas.RemoveFilter();
                    dtgvDatos.AutoGenerateColumns = false;
                    dtgvDatos.DataSource = _Marcas;
                    lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros encontrados";
                }
            }
            catch { }
        }

        public GestionMarcas()
        {
            InitializeComponent();
        }

        private void GestionMarcas_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txbFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            EdicionMarca f = new EdicionMarca();
            f.txbIdMarca.Text = dtgvDatos.CurrentRow.Cells["idmarca"].Value.ToString();
            f.txbMarc.Text = dtgvDatos.CurrentRow.Cells["marca"].Value.ToString();
            f.ShowDialog();
            CargarDatos();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionMarca f = new EdicionMarca();
            f.ShowDialog();
            CargarDatos();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Marca oMarca = new CLS.Marca();
                oMarca.IDMarca = int.Parse(dtgvDatos.CurrentRow.Cells["idmarca"].Value.ToString());
                if (oMarca.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
            }
        }
    }
}
