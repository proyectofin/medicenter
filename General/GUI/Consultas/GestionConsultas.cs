﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.Operaciones
{
    public partial class GestionConsultas : Form
    {
        public static class OPERACION
        {
            public static int VENTA = 1;
            public static int COMPRA = 2;
            public static int CONSULTA = 3;
        }

        int _Tipo_operacion;


        BindingSource _Consultas = new BindingSource();

        public int Tipo_operacion { get => _Tipo_operacion; set => _Tipo_operacion = value; }

        private void CargarConsultas()
        {
            try
            {
                 _Consultas.DataSource = CacheManager.SystemCache.TodasConsultas();
                FiltrarLocalmente();
            }
            catch { }
        }
        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Consultas.Filter = "servicio LIKE '%" + txbFiltro.Text + "%' OR medico LIKE '%" + txbFiltro.Text + "%' OR paciente LIKE '%" + txbFiltro.Text + "%'";
                    lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros encontrados";
                }
                else
                {
                    _Consultas.RemoveFilter();
                    dtgvDatos.AutoGenerateColumns = false;
                    dtgvDatos.DataSource = _Consultas;
                    lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros encontrados";
                }
            }
            catch { }
        }
        public GestionConsultas()
        {
            InitializeComponent();
        }

        private void GestionOperaciones_Load(object sender, EventArgs e)
        {
            CargarConsultas();
        }

        private void btnRegConsulta_Click(object sender, EventArgs e)
        {
            EdicionConsulta f = new EdicionConsulta();
            f.Show();
        }

        private void txbFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }
    }
}
