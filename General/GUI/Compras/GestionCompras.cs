﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.Compras
{
    public partial class GestionCompras : Form
    {
        BindingSource _Compras = new BindingSource();

        private void CargarCompras()
        {
            try
            {
                _Compras.DataSource = CacheManager.SystemCache.TodasCompras();
                FiltrarLocalmente();
            }
            catch { }
        }
        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Compras.Filter = "proveedor LIKE '%" + txbFiltro.Text + "%'";
                    lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros encontrados";
                }
                else
                {
                    _Compras.RemoveFilter();
                    dtgvDatos.AutoGenerateColumns = false;
                    dtgvDatos.DataSource = _Compras;
                    lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros encontrados";
                }
            }
            catch { }
        }

        public GestionCompras()
        {
            InitializeComponent();
        }

        private void GestionCompras_Load(object sender, EventArgs e)
        {
            CargarCompras();
        }

        private void txbFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnRegConsulta_Click(object sender, EventArgs e)
        {
            EdicionCompra f = new EdicionCompra();
            f.ShowDialog();
            CargarCompras();
        }
    }
}
